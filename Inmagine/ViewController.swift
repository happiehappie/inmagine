//
//  ViewController.swift
//  Inmagine
//
//  Created by Jack Xiong Lim on 25/2/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var dimensionSegmentedControl: UISegmentedControl!
  let canvas = CALayer()
  //ratio from 320
  let margin: CGFloat = 20
  var dimension: CGFloat = 2
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    self.view.layer.addSublayer(canvas)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    //color picked
    self.canvas.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1).cgColor
    //assume size
    self.canvas.bounds = CGRect(origin: CGPoint.zero, size: CGSize(width: 320, height: 320))
    self.canvas.position.x = self.view.bounds.midX
    self.canvas.position.y = self.view.bounds.midY
    
    self.changeDimension(self.dimensionSegmentedControl)
    
  }
  
  func removeSublayers(from layer: CALayer) {
    layer.sublayers = []
  }
  
  @IBAction func changeDimension(_ sender: UISegmentedControl) {
    self.removeSublayers(from: self.canvas)
    //assume dimension is only 2 or 3
    self.dimension = CGFloat(sender.selectedSegmentIndex + 2)
    Drawer.drawBoxes(with: dimension, margin: margin, canvas: self.canvas)
  }
  
  @IBAction func loadImage(_ sender: UIButton) {
    for n in 0..<(self.canvas.sublayers ?? []).count {
      Drawer.addImage(to: self.canvas.sublayers![n], index: n)
    }
    
  }
  
}

