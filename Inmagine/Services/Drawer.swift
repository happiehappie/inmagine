//
//  Drawer.swift
//  Inmagine
//
//  Created by Jack Xiong Lim on 25/2/20.
//  Copyright © 2020 JXLim. All rights reserved.
//

import Foundation
import UIKit
import MetalPetal

class Drawer {
  class func drawBoxes(with dimension: CGFloat, margin: CGFloat, canvas: CALayer) {
    for row in 0..<Int(dimension) {
      for column in 0..<Int(dimension) {
        let width = (canvas.bounds.width - CGFloat((margin  * (dimension + 1)))) / dimension
        let xOrigin = (margin * CGFloat(column + 1)) + (width * CGFloat(column))
        let yOrigin = (margin * CGFloat(row + 1)) + (width * CGFloat(row))
        let box = UIBezierPath(rect: CGRect(x: xOrigin, y: yOrigin, width: width, height: width))
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = box.cgPath
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.strokeColor = UIColor.black.cgColor
        canvas.addSublayer(shapeLayer)
      }
    }
    
  }
  
  class func addImage(to layer: CALayer, index: Int) {
    
    guard let image = UIImage(named: "\(index)"), let mtiImage = image.makeMTIImage() else { return }
    
    layer.sublayers = []
    
    let imageLayer = CALayer()
    layer.addSublayer(imageLayer)
    imageLayer.frame = ((layer as? CAShapeLayer)?.path!.boundingBox)!
    let vibranceFilter = MTIVibranceFilter()
    vibranceFilter.avoidsSaturatingSkinTones = true
    let claheFilter = MTICLAHEFilter()
    let filteredImage = FilterGraph.makeImage { output in
      mtiImage => vibranceFilter => claheFilter => output
    }
    if let device = MTLCreateSystemDefaultDevice(), let outputImage = filteredImage {
        do {
            let context = try MTIContext(device: device)
            let filteredImage = try context.makeCGImage(from: outputImage)
            imageLayer.contents = filteredImage
        } catch {
            print(error)
        }
    }
  }
  
}
